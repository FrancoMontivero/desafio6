import * as fs from 'fs';

export interface Product {
	title: string;
	price: number;
	thumbnail: string;
	id?: number;
}

export class FileTxt{
	readonly nameFile: string;

	constructor(nameFile: string) {
		this.nameFile = nameFile.indexOf('.txt') !== -1 ? nameFile : `${nameFile}.txt`;
	}

	read(): Promise<any> {
		return new Promise((resolve, reject) => {
			fs.readFile(this.nameFile, {encoding: 'utf-8', flag: 'a+'}, (err, data) => {
				if(err){
					reject(err);
				}else {
					resolve(data);
				}
			})
		})
	}	

	async save(product: Product): Promise<any> {
		try {
			const response = await this.read();
			let newId: number;
			if(!response) newId = 1;
			else newId = JSON.parse(response).length + 1 || 1;
			try {
				await fs.promises.writeFile(
					this.nameFile, 
					JSON.stringify([
						...(response ? JSON.parse(response) : []),
						{id: newId, ...product}])
				)
				console.log("Satisfactory product loading");
			}
			catch (err) {
				console.log(err);
			}
		}
		catch (err) {
			console.log(err);
		}
	}

	delete(): Promise<any> {
		return new Promise((resolve, reject) => {
			fs.unlink(this.nameFile, (err) => {
				if(err) reject(err);
				else resolve(`Satisfactory delete file: ${this.nameFile}`);
			})
		})
	}
}
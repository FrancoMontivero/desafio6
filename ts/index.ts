import { FileTxt, Product } from './FileText';

async function main(): Promise<any> {
    const test: Product[] = [                                                                                                                                                     
        {                                                                                                                                                    
          title: 'Escuadra',                                                                                                                                 
          price: 123.45,                                                                                                                                     
          thumbnail: 'https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png',                                                                                                                                                                                  
        },                                                                                                                                                   
        {                                                                                                                                                    
          title: 'Calculadora',                                                                                                                              
          price: 234.56,                                                                                                                                     
          thumbnail: 'https://cdn3.iconfinder.com/data/icons/education-209/64/calculator-math-tool-school-256.png',                                                                                                                                                                                        
        },                                                                                                                                                   
        {                                                                                                                                                    
          title: 'Globo Terráqueo',                                                                                                                          
          price: 345.67,                                                                                                                                     
          thumbnail: 'https://cdn3.iconfinder.com/data/icons/education-209/64/globe-earth-geograhy-planet-school-256.png',                                   
        }                                                                                                                                                    
    ] 
    const file = new FileTxt('productos');

    for(let product of test){
        await file.save(product);
    }
    
    await file.read()
    .then(response => {
        console.log("Data final");
        console.log(response);
    })
    .catch(err => {
        console.log(err.message);
    })

    try {
        const response = await file.delete();
        console.log(response)
    }
    catch (err) {
        console.log("Hubo un error al eliminar el archivo")
    }
}

main()